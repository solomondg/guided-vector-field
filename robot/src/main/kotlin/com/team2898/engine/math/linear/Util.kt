package com.team2898.engine.math.linear

import org.apache.commons.math3.linear.*

fun rotateVector2D(source: Vector2D, rotation: Vector2D): Vector2D {
    if (source == Vector2D.ZERO) return source
    // We need to turn the first vector into a column vector (Array2DRowRealMatrix)
    // We need to turn the second vector into a rotation matrix in the form of
    //| cos(θ) -sin(θ) |
    //| sin(θ)  cos(θ) |
    // We assume that the vectors are in the form <cos(θ), sin(θ)>
    val sourceMatrix = Matrix(source.toArray())
    val normRot = rotation.normalize()
    val rotated = Matrix(
            arrayOf(
                    doubleArrayOf(normRot.x, -normRot.y),
                    doubleArrayOf(normRot.y, normRot.x)
            )
    ) * sourceMatrix
    return Vector2D(rotated.getColumn(0))
}

typealias Matrix = org.apache.commons.math3.linear.Array2DRowRealMatrix
typealias Vector2D = org.apache.commons.math3.geometry.euclidean.twod.Vector2D

fun row(vararg elem: Double) = doubleArrayOf(*elem)

// Row, col
fun RealMatrix.dim(): Pair<Int, Int> {
    return Pair(this.rowDimension, this.columnDimension)
}

operator fun RealMatrix.plus(other: RealMatrix) = add(other)
operator fun RealMatrix.minus(other: RealMatrix) = subtract(other)
operator fun RealMatrix.times(other: RealMatrix) = multiply(other)
operator fun RealMatrix.get(row: Int, col: Int) = getEntry(row, col)
operator fun RealMatrix.set(row: Int, col: Int, value: Double) = setEntry(row, col, value)

operator fun RealMatrix.times(other: Double) = scalarMultiply(other)
operator fun RealMatrix.plus(other: Double) = scalarAdd(other)

fun RealMatrix.T() = transpose()

//operator fun RealMatrix.times(other: Double) =
//        this.multiply(DiagonalMatrix(
//                DoubleArray(this.columnDimension) { i -> other }
//        ))


/** Ax = b, solve for x
 *
 */
fun solveLinearEquation(A: Matrix, b: Vector): Vector =
        Vector(LUDecomposition(A).solver.solve(b))

