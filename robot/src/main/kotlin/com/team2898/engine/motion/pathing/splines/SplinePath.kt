package com.team2898.engine.motion.pathing.splines

import com.team2898.engine.kinematics.Pose2d

class SplinePath(val spline: ISpline, val endPointTolerance: Double, val waypointSwitchoverTolerance: Double = 0.0) {
    var splineIndex = 0
    var splineLength = spline.length
    val currentSegment: ISplineSegment
        get() = spline[splineIndex]
    val tabledSegments: Pair<ISplineSegment, ISplineSegment>
        get() = Pair(spline[splineIndex], spline[splineIndex + 1])

    fun update(robotPose: Pose2d) {
        val currentSegProjErr = tabledSegments.first.projectPoint(robotPose.translation).error
        val nextSegProjErr = tabledSegments.second.projectPoint(robotPose.translation).error
        if (nextSegProjErr < currentSegProjErr) { // Time to switch to the next segment
            splineIndex++
        }
    }

}