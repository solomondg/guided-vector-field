package com.team2898.engine.motion.pathing.splines

import com.team2898.engine.extensions.Vector2D.div
import com.team2898.engine.extensions.Vector2D.minus
import com.team2898.engine.extensions.Vector2D.plus
import com.team2898.engine.extensions.Vector2D.times
import com.team2898.engine.kinematics.Translation2d
import com.team2898.engine.math.clamp
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D

class LineSegment(override val startPoint: Translation2d, override val endPoint: Translation2d) : IBiarcComponent {
    override fun pointToPointLength(startT: Double, endT: Double) = length * (endT - startT)
    override fun getPoint(t: Double) = startPoint.translateBy(slope * length * t)

    override fun projectPoint(point: Translation2d): projResult {
        throw NotImplementedError()
    }

    override fun getTangent(t: Double) = slope
    override fun getTangentDeriv(t: Double) = Vector2D(0.0, 0.0)
    override fun getCurvature(t: Double): Double = 0.0
    override val length = (startPoint.position - endPoint.position).norm

    val slope = (startPoint.position - endPoint.position).normalize()

}