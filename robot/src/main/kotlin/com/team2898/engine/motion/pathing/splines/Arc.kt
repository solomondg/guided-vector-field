package com.team2898.engine.motion.pathing.splines

import com.team2898.engine.extensions.Vector2D.times
import com.team2898.engine.kinematics.Rotation2d
import com.team2898.engine.kinematics.Translation2d
import com.team2898.engine.math.linear.Vector2D
import org.apache.commons.math3.analysis.interpolation.MicrosphereProjectionInterpolator
import kotlin.math.*


class Arc(val center: Translation2d, val radius: Double,
          val startAngle: Rotation2d, val endAngle: Rotation2d,
          val direction: Boolean = true) : IBiarcComponent {

    val angle = endAngle.theta - startAngle.theta

    override val length = abs(radius * angle)

    override val startPoint: Translation2d =
            center.translateBy(startAngle.rotation * radius)
    override val endPoint: Translation2d =
            center.translateBy(endAngle.rotation * radius)

    override fun pointToPointLength(startT: Double, endT: Double): Double =
            length * (endT - startT)

    override fun getPoint(t: Double): Translation2d =
            center + startAngle.rotateBy(Rotation2d.createFromRadians(angle * t)).rotation * radius

    override fun projectPoint(point: Translation2d): projResult {
        // Need to find the points on the circle where our projecting point lies on the normal at that point
        // (if that makes sense lol)
        // aka there exists an x,t where x*getNormal(t) == point

        // if radius is R, origin is (0,0), point is(x,y)
        // intersection point in polar is (R, atan(y/x))
        val centeredPoint = point - center
        val projectionPointPolar = Vector2D(radius, atan2(centeredPoint.y, centeredPoint.x))
        val projectedPointCartesian = Vector2D(
                projectionPointPolar.x * cos(projectionPointPolar.y),
                projectionPointPolar.x * sin(projectionPointPolar.y)
        )
        val projTheta = Rotation2d(projectedPointCartesian).radians
        val mirrorProjTheta = projTheta + PI % (2 * PI) // TODO: Optimize xd
        //println("Proj theta: $projTheta")
        //println("Mirror theta: $mirrorProjTheta")

        val proj = Rotation2d.createFromRadians(projTheta)
        val mirrorProj = Rotation2d.createFromRadians(mirrorProjTheta)
        //println("Proj: $proj")
        //println("Mirror: $mirrorProj")
        val angleToStart = acos((proj.rotation * startAngle.rotation) / (proj.rotation.norm * startAngle.rotation.norm))
        val angleToEnd = acos((proj.rotation * endAngle.rotation) / (proj.rotation.norm * endAngle.rotation.norm))
        val mirrorAngleToStart = acos((mirrorProj.rotation * startAngle.rotation) / (mirrorProj.rotation.norm * startAngle.rotation.norm))
        val mirrorAngleToEnd = acos((mirrorProj.rotation * endAngle.rotation) / (mirrorProj.rotation.norm * endAngle.rotation.norm))

        val mirrorInRange: Boolean
        val normalInRange: Boolean
        normalInRange = (angleToStart < angle && angleToEnd < angle)
        mirrorInRange = (mirrorAngleToStart < angle && mirrorAngleToEnd < angle)

        //println("Normal to start: $angleToStart")
        //println("Normal to end: $angleToEnd")
        //println("Mirror to start: $mirrorAngleToStart")
        //println("Mirror to end: $mirrorAngleToEnd")
        //println("Normal in range: $normalInRange")
        //println("Mirror in range: $mirrorInRange")


        //val e = projResult(point = Translation2d(projectedPointCartesian))
        // TODO: Need to do stuff with the center so it's not all subtractified

        val point: Rotation2d
        point = if (normalInRange) {
            println("Std projection")
            proj
        } else if (mirrorInRange) {
            println("Mirror projection")
            mirrorProj
        } else {
            val normalError = min(angleToStart, angleToEnd)
            val mirrorError = min(mirrorAngleToStart, mirrorAngleToEnd)
            if (normalError > mirrorError) {
                println("Std projection (err) ")
                proj
            } else {
                println("Mirror projection (err) ")
                mirrorProj
            }
        }
        //center + startAngle.rotateBy(Rotation2d.createFromRadians(angle * t)).rotation * radius

        val tEquals0 = getPoint(0.0) - center
        // point = startAngle.rotateBy(Rotation2d.createFromRadians(angle*t)).rotation * radius
        // Find t

        // point = [cos(t) -sin(t)] * [cos(t0)] * radius
        //         [sin(t)  cos(t)]   [sin(t0)]
        // point = [cos(t) cos(t0) - sin(t) sin(t0)] * radius
        //         [sin(t) cos(t0) + cos(t) sin(t0)]
        // point.x = [radius * cos(t) * cos(t0) - radius * sin(t) * sin(t0)]
        // point.y = [radius * sin(t) * cos(t0) + radius * cos(t) * sin(t0)]
        // a = cos(t0)
        // b = sin(t0)
        // c = radius
        // point.x = [c*cos(t)*a - c*sin(t)*b]
        // point.y = [c*sin(t)*a + c*cos(t)*b]


        throw NotImplementedError()
    }

    override fun getTangent(t: Double): Vector2D {
        // tangent of a circle
        // circle is [cos sin]
        // tan (deriv) is [-sin cos]
        val point = Rotation2d((getPoint(t) - center).position).normal.rotation
        return point
    }

    override fun getTangentDeriv(t: Double): Vector2D {
        // hessian of a circle
        // circle is [cos sin]
        // deriv is [-sin cos]
        // hessian is [-cos -sin]
        //          [x y] -> [-y x]
        val point = getTangent(t)
        return Vector2D(-point.y, point.x)
    }

    override fun getNormal(t: Double): Vector2D {
        val point = getTangent(t)
        return Vector2D(point.y, -point.x)
    }

    override fun getCurvature(t: Double) = if (radius != 0.0) 1 / radius else 0.0

}