package com.team2898.engine.motion.pathing.splines

import com.team2898.engine.kinematics.Translation2d
import com.team2898.engine.math.linear.Matrix
import com.team2898.engine.math.linear.T
import com.team2898.engine.math.linear.Vector
import com.team2898.engine.math.linear.times
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D
import kotlin.math.pow


class CubicPolynomial(ax: Double, bx: Double, cx: Double, dx: Double,
                      ay: Double, by: Double, cy: Double, dy: Double) : ISplineSegment {

    private data class Coeffs(val a: Double, val b: Double, val c: Double, val d: Double) : Vector(a, b, c, d)

    private val X = Coeffs(ax, bx, cx, dx)
    private val Y = Coeffs(ay, by, cy, dy)
    private val dX = Coeffs(0.0, 3 * ax, 2 * bx, cx)
    private val dY = Coeffs(0.0, 3 * ax, 2 * bx, cx)
    private val d2X = Coeffs(0.0, 0.0, 6 * ax, 2 * bx)
    private val d2Y = Coeffs(0.0, 0.0, 6 * ay, 2 * by)

    override val length: Double
        get() = throw NotImplementedError()

    override fun pointToPointLength(startT: Double, endT: Double): Double =
            throw NotImplementedError()

    override fun projectPoint(point: Translation2d): projResult =
            throw NotImplementedError()

    override val startPoint = Translation2d(dx, dy)
    override val endPoint = Translation2d(ax + bx + cx + dx, ay + by + cy + dy)

    private fun T(t: Double) = Vector(t.pow(3), t.pow(2), t.pow(1), t.pow(0))

    override fun getPoint(t: Double) = getPoint(t, X, Y)

    private fun getPoint(t: Double, X: Vector, Y: Vector) = Translation2d(
            X.dotProduct(T(t)), Y.dotProduct(T(t))
    )

    override fun getTangent(t: Double): Vector2D = Vector2D(getPoint(t, dX, dY).x, getPoint(t, dX, dY).y)

    override fun getTangentDeriv(t: Double): Vector2D = Vector2D(getPoint(t, d2X, d2Y).x, getPoint(t, d2X, d2Y).y)

}