package com.team2898.engine.motion.pathing.splines

import com.team2898.engine.kinematics.Pose2d

interface ISpline {
    val segments: List<ISplineSegment>
    val waypoints: List<Pose2d>
    val length: Double
        get() = segments.sumByDouble { it.length }
}

operator fun ISpline.get(index: Int) = segments[index]
