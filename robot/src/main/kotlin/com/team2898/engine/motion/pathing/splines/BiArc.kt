package com.team2898.engine.motion.pathing.splines

import com.team2898.engine.extensions.Vector2D.minus
import com.team2898.engine.extensions.Vector2D.plus
import com.team2898.engine.extensions.Vector2D.rotate
import com.team2898.engine.extensions.Vector2D.times
import com.team2898.engine.kinematics.Rotation2d
import com.team2898.engine.kinematics.Translation2d
import com.team2898.engine.math.linear.Vector
import com.team2898.engine.math.linear.Vector2D
import kotlin.math.PI
import kotlin.math.abs
import kotlin.math.acos
import kotlin.math.sqrt


class BiArc(override val startPoint: Translation2d,
            override val endPoint: Translation2d,
            val startTangent: Rotation2d,
            val endTangent: Rotation2d) : ISplineSegment {


    val arc1: IBiarcComponent
    val arc2: IBiarcComponent

    init {
        val components = ComputeBiarc(startPoint = startPoint,
                endPoint = endPoint,
                startTangent = startTangent,
                endTangent = endTangent)
        arc1 = components.first
        arc2 = components.second
    }

    override val length = arc1.length + arc2.length

    override fun pointToPointLength(startT: Double, endT: Double): Double {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getPoint(t: Double): Translation2d {
        return if (t < 0.5) arc1.getPoint(t * 2)
        else arc2.getPoint(t * 2)
    }

    override fun projectPoint(point: Translation2d): projResult {
        throw NotImplementedError()
    }

    override fun getTangent(t: Double): Vector2D {
        return if (t < 0.5) arc1.getTangent(t * 2)
        else arc2.getTangent(t * 2)
    }

    override fun getTangentDeriv(t: Double): Vector2D {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private infix fun Vector2D.cross(b: Vector2D) =
            this.x * b.y - this.y * b.x

//fun cross(a: Vector2D, b: Vector2D) = a.x * b.y - a.y * b.x


    fun ComputeBiarc(startPoint: Translation2d, endPoint: Translation2d,
                     startTangent: Rotation2d, endTangent: Rotation2d): Pair<IBiarcComponent, IBiarcComponent> {


        // have p1, p2, t1, t2
        // need c1, c2, r1, r2, θ1, θ2, pm


        // Let v = p2-p1
        // Let t = t1+t2
        val p1 = startPoint
        val p2 = endPoint
        val t1 = startTangent
        val t2 = endTangent
        val v = p2 - p1
        val t = t1.rotation + t2.rotation


        data class BiarcParams(val c1: Translation2d,
                               val c2: Translation2d,
                               val pm: Translation2d,
                               val r1: Double,
                               val r2: Double,
                               val θ1: Double,
                               val θ2: Double)

        val params: BiarcParams

        fun calcFromD2(d2: Double): BiarcParams {
            val d1 = d2
            val pm = (p1 + p2 + (t1.rotation - t2.rotation) * d2) / 2.0

            val n1 = Vector2D(-t1.rotation.y, t1.rotation.x)
            val n2 = Vector2D(-t2.rotation.y, t2.rotation.x)
            val s1 = ((pm - p1).position * (pm - p1).position) / ((n1 * 2.0) * (pm - p1).position)
            val s2 = ((pm - p2).position * (pm - p2).position) / ((n2 * 2.0) * (pm - p2).position)

            val r1 = abs(s1)
            val r2 = abs(s2)

            val c1 = p1 + n1 * s1
            val c2 = p2 + n2 * s2

            val θ1: Double
            val θ2: Double

            val op1 = ((p1 - c1) / r1).position
            val om1 = ((pm - c1) / r1).position
            val op2 = ((p2 - c2) / r2).position
            val om2 = ((pm - c2) / r2).position

            θ1 = when {
                (r1 == 0.0) -> 0.0
                (d1 > 0 && op1 cross om1 > 0) -> acos(op1 * om1)
                (d1 > 0 && op1 cross om1 <= 0) -> -acos(op1 * om1)
                (d1 <= 0 && op1 cross om1 > 0) -> -2 * PI + acos(op1 * om1)
                (d1 <= 0 && op1 cross om1 <= 0) -> 2 * PI - acos(op1 * om1)
                else -> 0.0
            }

            θ2 = when {
                (r2 == 0.0) -> 0.0
                (d2 > 0 && op2 cross om2 > 0) -> acos(op2 * om2)
                (d2 > 0 && op2 cross om2 <= 0) -> -acos(op2 * om2)
                (d2 <= 0 && op2 cross om2 > 0) -> -2 * PI + acos(op2 * om2)
                (d2 <= 0 && op2 cross om2 <= 0) -> 2 * PI - acos(op2 * om2)
                else -> 0.0
            }

            return BiarcParams(c1, c2, pm, r1, r2, θ1, θ2)
        }

        fun calcParams(): BiarcParams {

            // Quadratic function time
            val denom = 2 * (1 - t1.rotation * t2.rotation)
            val numer = -(v.position * t) +
                    sqrt(
                            Math.pow(v.position * t, 2.0) +
                                    2 * (1 - t1.rotation * t2.rotation) * (v.position * v.position))

            if (denom > 0) {
                println("Case 1")
                // Case 1
                // No tangents equal
                val d2 = numer / denom
                println("D2: $d2")
                return calcFromD2(d2)

            } else {
                // For 2(1-t1*t2) == 0, t1 needs to equal t2 (t1*t2==1), so no checking needed for t1==t2
                if (v.normalized * t1.rotation == 0.0 && v.normalized * t2.rotation == 0.0) {
                    println("Case 3")
                    // Case 3

                    // Both tangents equal, v is perp
                    // Will cause singularity in denominator of d2 in case 2
                    // (implying that d1 == d2 == inf)
                    val pm = p1 + v / 2.0
                    val c1 = p1 + v / 4.0
                    val c2 = p1 + (v / 4.0) * 3.0
                    val r1 = v.norm / 4
                    val r2 = r1
                    val θ1 = if (v.position cross t2.rotation < 0) PI else -PI
                    val θ2 = if (v.position cross t2.rotation > 0) PI else -PI
                    return BiarcParams(c1, c2, pm, r1, r2, θ1, θ2)
                } else {
                    println("Case 2")
                    // Case 2
                    val d2 = (v.position * v.position) / (v.position * t2.rotation * 4)
                    println("D2: $d2")
                    return calcFromD2(d2)
                }
            }
        }

        params = calcParams()
        val pm = params.pm
        val c1 = params.c1
        val c2 = params.c2
        val r1 = params.r1
        val r2 = params.r2
        val θ1 = params.θ1
        val θ2 = params.θ2

        val firstCenter = c1
        val secondCenter = c2
        val firstRadius = r1
        val secondRadius = r2
        val firstStartAngle = startTangent
        val secondEndAngle = endTangent

        val firstEndAngle = Rotation2d.createFromRadians(firstStartAngle.radians + θ1)
        val secondStartAngle = firstEndAngle

        val arc1 = Arc(firstCenter, firstRadius, firstStartAngle.normal, firstEndAngle.normal)
        val arc2 = Arc(secondCenter, secondRadius, secondStartAngle.normal, secondEndAngle.normal)

        //println("$firstCenter\n$firstRadius\n$firstStartAngle\n$firstEndAngle\n\n")
        //println("$secondCenter\n$secondRadius\n$secondStartAngle\n$secondEndAngle")

        //println("\n\n$pm\n$c1\n$c2\n$r1\n$r2\n$θ1\n$θ2\n\n")
        //println("$firstStartAngle\n$firstEndAngle\n$secondStartAngle\n$secondEndAngle")

        //println("${arc1.startPoint}")
        //println("${arc1.endPoint}")
        //println("${arc2.startPoint}")
        //println("${arc2.endPoint}")

        return Pair(arc1, arc2)
    }

}
