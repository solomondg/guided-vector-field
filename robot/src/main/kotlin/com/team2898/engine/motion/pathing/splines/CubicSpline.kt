package com.team2898.engine.motion.pathing.splines

import com.team2898.engine.kinematics.Pose2d
import com.team2898.engine.math.linear.Matrix
import com.team2898.engine.math.linear.T
import com.team2898.engine.math.linear.Vector
import com.team2898.engine.math.linear.solveLinearEquation
import org.apache.commons.math3.linear.MatrixUtils


class CubicSpline(override val waypoints: List<Pose2d>) : ISpline {

    override val segments: List<ISplineSegment>

    init {
        //                    ax   bx   cx   dx   ay   by   cy   dy
        val coeffs = Matrix(arrayOf(
                doubleArrayOf(0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0), //s.x
                doubleArrayOf(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0), //s.y
                doubleArrayOf(1.0, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0), //e.x
                doubleArrayOf(0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0), //e.y
                doubleArrayOf(0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0), //s.c
                doubleArrayOf(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0), //s.s
                doubleArrayOf(3.0, 2.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0), //e.c
                doubleArrayOf(0.0, 0.0, 0.0, 0.0, 3.0, 2.0, 1.0, 0.0)  //e.s
        ))

        // Time to reticulate
        segments = List(waypoints.size - 1) { i ->
            val startPose = waypoints[i]
            val endPose = waypoints[i + 1]
            val goal = Vector(
                    startPose.x, startPose.y, endPose.x, endPose.y,
                    startPose.cos, startPose.sin, endPose.cos, endPose.sin
            )
            val solution: Vector = solveLinearEquation(A = coeffs, b = goal)
            val (ax, bx, cx, dx) = solution.getSubVector(0, 4).toArray()
            val (ay, by, cy, dy) = solution.getSubVector(3, 4).toArray()
            CubicPolynomial(ax, bx, cx, dx, ay, by, cy, dy)
        }
    }
}