package com.team2898.engine.motion.pathing.splines

import com.team2898.engine.extensions.Vector2D.times
import com.team2898.engine.kinematics.Translation2d
import com.team2898.engine.math.linear.Matrix
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D

data class projResult(val point: Translation2d, val t: Double, val error: Double)

val normalTransformMatrix = Matrix(arrayOf(
        doubleArrayOf(0.0, -1.0),
        doubleArrayOf(1.0, 0.0)
))

interface ISplineSegment {
    val length: Double
    val startPoint: Translation2d
    val endPoint: Translation2d
    fun pointToPointLength(startT: Double, endT: Double): Double
    fun getPoint(t: Double): Translation2d
    fun projectPoint(point: Translation2d): projResult
    fun getTangent(t: Double): Vector2D
    fun getNormal(t: Double): Vector2D = getTangent(t) * normalTransformMatrix
    fun getTangentDeriv(t: Double): Vector2D
    fun getCurvature(t: Double): Double = getTangentDeriv(t).normalize().norm / getTangent(t).normalize().norm
}