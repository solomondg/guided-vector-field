package com.team2898.robot.rarted

import com.team2898.engine.math.linear.Matrix
import org.apache.commons.math3.linear.Array2DRowRealMatrix
import org.apache.commons.math3.linear.MatrixUtils

fun Matrix.`⌹`(): Matrix =
        MatrixUtils.inverse(this) as Matrix


val whatever = Matrix().`⌹`()
