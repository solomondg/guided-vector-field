package com.team2898.robot

val leftCIM = CIMModel()
val rightCIM = CIMModel()

inline fun setVoltage(voltages: Pair<Double, Double>) {
    leftCIM.inputVoltage = voltages.first
    rightCIM.inputVoltage = voltages.second
}