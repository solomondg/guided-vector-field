package com.team2898.robot

import com.team2898.engine.async.AsyncLooper
import com.team2898.engine.extensions.div
import com.team2898.engine.extensions.times
import com.team2898.robot.config.PhysicalConf.wheelBaseWidthMeters
import com.team2898.robot.config.PhysicalConf.wheelDiameterMeters
import com.team2898.robot.config.PhysicalConf.wheelGearRatio
import edu.wpi.first.wpilibj.PIDController
import edu.wpi.first.wpilibj.PIDSource
import edu.wpi.first.wpilibj.PIDSourceType

object DTcontroller {

    var desiredLinear = 0.0 // in m/s, feedforward only
    var desiredHeading = 0.0 // -pi to pi, PD control only
        set(value) {
            field = value
            pid.setpoint = value
        }

    private var pidAngVel = 0.0

    init {
        leftCIM // Make sure these guys are nice and instantiated
        rightCIM
    }

    val pid = PIDController(
            0.0, 0.0, 0.0, 0.0,
            object : PIDSource {
                override fun getPIDSourceType(): PIDSourceType = PIDSourceType.kDisplacement
                override fun setPIDSourceType(pidSource: PIDSourceType) {}
                override fun pidGet(): Double =
                        Kinematics.pose.theta
            },
            { output -> pidAngVel = output }
    )

    val loop = AsyncLooper(50.0) {
        // angVel = (Dr - Dl) / wheelBase
        // linVel = (Dl + Dr)/2
        // Dr = (wheelBase*angVel + 2*linVel)/2
        // Dl = (-wheelBase*angVel + 2*linVel)/2
        //println("Receiving command velocity subscription")
        //println("Commanded angular vel: $angularVel")
        //println("Commanded linear vel: $linVel")
        val linVel = desiredLinear
        val angularVel = pidAngVel
        val lSpeedMsec = (-wheelBaseWidthMeters * angularVel + 2 * linVel) / 2.0
        val rSpeedMsec = (wheelBaseWidthMeters * angularVel + 2 * linVel) / 2.0
        val targetSpeedsMsec = Pair(lSpeedMsec, rSpeedMsec)
        val targetSpeedsRotSec = targetSpeedsMsec / (wheelDiameterMeters * Math.PI)
        val targetSpeedsMotorRotSec = targetSpeedsRotSec / wheelGearRatio
        val targetSpeedsMotorRPM = targetSpeedsMotorRotSec * 60.0
        val targetMotorVoltages = targetSpeedsMotorRPM * (12.0 / 5310)
        leftCIM.inputVoltage = targetMotorVoltages.first
        rightCIM.inputVoltage = targetMotorVoltages.second
    }
}