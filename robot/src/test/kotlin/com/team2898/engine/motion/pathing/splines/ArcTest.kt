package com.team2898.engine.motion.pathing.splines

import com.team2898.engine.kinematics.Pose2d
import com.team2898.engine.kinematics.Rotation2d
import com.team2898.engine.kinematics.Translation2d
import com.team2898.engine.math.linear.Matrix
import com.team2898.engine.math.linear.row
import io.kotlintest.Description
import io.kotlintest.TestResult
import io.kotlintest.shouldBe
import io.kotlintest.specs.StringSpec
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.runBlocking
import org.apache.commons.math3.linear.LUDecomposition
import java.io.File
import kotlin.math.sqrt

class ArcTest : StringSpec() {
    init {
        // Defines an arc that starts from 0,1  and goes to 1,0
        val arc = Arc(
                center = Translation2d(0.0, 0.0),
                radius = 1.0,
                startAngle = Rotation2d(0.0, 1.0),
                endAngle = Rotation2d(1.0, 0.0))
        //val arc = Arc(
        //        center = Translation2d(0.0, 0.0),
        //        radius = 1.0,
        //        startTangent = Rotation2d(-1.0, 0.0),
        //        endTangent = Rotation2d(0.0, 1.0))

        //println("len: ${arc.length}")
        ////println("start theta: ${arc.startTangent.theta}")
        ////println("end theta: ${arc.endTangent.theta}")

        //println(" ")

        //println("0.0 pos: ${arc.getPoint(0.0)}")
        //println("0.5 pos: ${arc.getPoint(0.5)}")
        //println("1.0 pos: ${arc.getPoint(1.0)}")
        //println("Start point: ${arc.startPoint}")
        //println("End point: ${arc.endPoint}")

        //println(" ")

        //println("0.0 tan: ${arc.getTangent(0.0)}")
        //println("0.5 tan: ${arc.getTangent(0.5)}")
        //println("1.0 tan: ${arc.getTangent(1.0)}")

        //println(" ")

        //println("0.0 hess: ${arc.getTangentDeriv(0.0)}")
        //println("0.5 hess: ${arc.getTangentDeriv(0.5)}")
        //println("1.0 hess: ${arc.getTangentDeriv(1.0)}")

        //println(" ")

        //println("0.0 norm: ${arc.getNormal(0.0)}")
        //println("0.5 norm: ${arc.getNormal(0.5)}")
        //println("1.0 norm: ${arc.getNormal(1.0)}")
    }
}


class ArcTest2 : StringSpec() {
    init {
        println("\n\n\n")
        println("Arc test 2")
        val startPoint = Translation2d(0.0, 1.0)
        val midPoint = Translation2d(-1.0, 0.0)
        val endPoint = Translation2d(1.0, 0.0)

        val hNum = Matrix(arrayOf(
                row(startPoint.norm, startPoint.y, 1.0),
                row(midPoint.norm, midPoint.y, 1.0),
                row(endPoint.norm, endPoint.y, 1.0)
        ))
        val denomMat = Matrix(arrayOf(
                row(startPoint.x, startPoint.y, 1.0),
                row(midPoint.x, midPoint.y, 1.0),
                row(endPoint.x, endPoint.y, 1.0)
        ))
        val denom = 2 * LUDecomposition(denomMat).determinant
        val h = LUDecomposition(hNum).determinant / denom

        val kNum = Matrix(arrayOf(
                row(startPoint.x, startPoint.norm, 1.0),
                row(midPoint.x, midPoint.norm, 1.0),
                row(endPoint.x, endPoint.norm, 1.0)
        ))
        val k = LUDecomposition(kNum).determinant / denom

        val center = Translation2d(h, k)
        val radius = (center - startPoint).norm
        val beginAngle = Rotation2d((startPoint - center).position)
        val endAngle = Rotation2d((endPoint - center).position)

        val arc = Arc(center, radius, beginAngle, endAngle)
        //println(center)
        //println(radius)
        //println(beginAngle)
        //println(endAngle)
    }
}


class ArcTest3 : StringSpec() {
    init {
        val startPoint = Translation2d(0.0, 0.0)
        val endPoint = Translation2d(2.0, 2.0)
        val startTangent = Rotation2d(1.0, 0.0)
        val endTangent = Rotation2d(1.0, 1.0)

        val biarc = BiArc(
                startPoint = startPoint,
                endPoint = endPoint,
                startTangent = startTangent,
                endTangent = endTangent
        )

        var csvList = ArrayList<Pair<Double, Translation2d>>()
        for (i in 0..100) {
            val t = i / 100.0
            csvList.add(Pair(t, biarc.arc1.getPoint(t)))
        }
        for (i in 0..100) {
            val t = i / 100.0
            csvList.add(Pair(t + 1, biarc.arc2.getPoint(t)))
        }

        var string = ""

        csvList.forEach { string += "${it.first},${it.second.x},${it.second.y}\n" }
        File("/home/solomon/output.csv").writeText(string)

        //println(biarc.arc1.startAngle.rotation)
        //println(biarc.arc1.endAngle.rotation)
        //println(biarc.arc2.startAngle.rotation)
        //println(biarc.arc2.endAngle.rotation)
    }
}

class TPose2d : Pose2d() {
    init {
        runBlocking {
            launch {
                async {
                    System.exit(0)
                }.await()
            }.join()
        }
    }
}

class ArcTest4 : StringSpec() {
    init {
        val arc = Arc(
                center = Translation2d(0.0, 0.0),
                radius = 1.0,
                startAngle = Rotation2d(1.0, 0.0),
                endAngle = Rotation2d(0.0, 1.0)
        )

        //TPose2d()
        //arc.projectPoint(Translation2d(0.5, -0.75))
        arc.projectPoint(Translation2d(-0.1, -0.3))
    }
}

