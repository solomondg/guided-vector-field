from typing import List

from util.Point import Point2D
from util.QuinticPolynomial import QuinticPolynomial
from util.SplineGVF import SplineGVF


class PathSegmentManager:
    splineSize: int
    splines: List[QuinticPolynomial]

    def __init__(self, spline: List[QuinticPolynomial]):
        self.splines = spline
        self.splineSize = len(self.splines)
        self.currentSplineIndex = 0

    def completed(self) -> bool:
        return self.currentSplineIndex == self.splineSize-1

    # TODO: Unfuckify this - need to be able to uncap the 0,1 bounds in projection method so we can detect if we're off
    # the spline? Or just see if this works as is
    def update(self, robotPoint: Point2D):
        assert type(robotPoint) == Point2D
        if not self.completed():
            # God have mercy
            firstProjError = SplineGVF(spline=self.getTabledSegments()[0], Kn=0, Ks=0).project(robotPoint)[2]
            secondProjError = SplineGVF(spline=self.getTabledSegments()[1], Kn=0, Ks=0).project(robotPoint)[2]
            if secondProjError < firstProjError:
                self.currentSplineIndex += 1
                print("--------------- INCREMENTING SPLINE ------------")

            print("SPLINE",self.currentSplineIndex,"OF",self.splineSize-1)

    def getTabledSegments(self) -> List[QuinticPolynomial]:
        return [self.splines[self.currentSplineIndex], self.splines[self.currentSplineIndex + 1]]

    def getCurrentSegment(self) -> QuinticPolynomial:
        return self.splines[self.currentSplineIndex]
