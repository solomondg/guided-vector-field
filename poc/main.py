import sys
import time

import numpy as np
from matplotlib.figure import Figure
from matplotlib.lines import Line2D

from PathFollower import PathFollower
from SimRobot import SimRobot
from util.Point import Waypoint, Point2D

import matplotlib

matplotlib.use('agg')
print(matplotlib.get_backend())

waypoints = [
    Waypoint(0, 0, 0),
    Waypoint(1, 0, 1),
    Waypoint(2, 1, 0.78),
    Waypoint(3, 2, 0.78),
    Waypoint(4,2,0),
    Waypoint(5,2,0)
    # Waypoint(6, 3, -1.4),
    # Waypoint(0, -2, -3.1),
    # Waypoint(0, 0, 0)
]

robotSim = SimRobot(startPoint=Point2D(0.2, 0.1),constSpeed=5)

follower = PathFollower(waypoints=waypoints,
                        setTargetHeadingLam=lambda h: robotSim.setHeading(h),
                        getRobotPositionLam=lambda: robotSim.currentPoint)

print("splines len", len(follower.segMngr.splines))

lineColor = '#690a0f'
fig = Figure(figsize=(13, 9), dpi=90)
plot = fig.add_subplot(111)

t = 0

csv = ""
while not follower.isFinished():
    start = time.time()
    follower.update()
    robotSim.update()
    t += 0.1
    print(round(t, 4), robotSim.currentPoint, robotSim.currentHeading)

    robotX, robotY, robotU, robotV = \
        robotSim.currentPoint.x, robotSim.currentPoint.y, robotSim.currentHeading.x, robotSim.currentHeading.y
    csv += str(robotX) + "," + str(robotY) + "\n"
    #print("dt", time.time() - start)

with open('robotTrack.csv', 'w') as f:
    f.write(csv)
