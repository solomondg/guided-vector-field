import numpy as np

from util.Point import Point2D, Vector2D, toVec2D


class SimRobot:
    currentPoint: Point2D
    currentHeading: Vector2D
    speed: float

    def __init__(self, startPoint: Point2D = Point2D(0, 0), startHeading: Vector2D = Vector2D(1, 0), constSpeed=0.5):
        self.currentPoint: Point2D = startPoint
        self.currentHeading = startHeading
        self.speed = constSpeed

    def update(self, dt=0.01):
        self.currentPoint.x += dt * self.speed * self.currentHeading.x
        self.currentPoint.y += dt * self.speed * self.currentHeading.y

    def setHeading(self, h: Vector2D):
        self.currentHeading = h

    def getPosition(self) -> Point2D:
        return self.currentPoint
