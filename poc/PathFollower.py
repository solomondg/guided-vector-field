from typing import List, Callable

from PathSegmentManager import PathSegmentManager
from util.Point import Waypoint, Point2D, Vector2D
from util.QuinticPolynomial import QuinticPolynomial
from util.QuinticSpline import QuinticSpline
from util.SplineGVF import SplineGVF

Kn = 24
Ks = 24


class PathFollower:
    gvf: SplineGVF
    splines: List[QuinticPolynomial]
    segMngr: PathSegmentManager
    setTargetHeading: Callable
    getRobotPosition: Callable
    currentSegment: QuinticPolynomial

    def __init__(self, waypoints: List[Waypoint], setTargetHeadingLam: Callable, getRobotPositionLam: Callable):
        self.splines = QuinticSpline(waypoints=waypoints).splines
        self.gvf = SplineGVF(spline=self.splines[0], Ks=Ks, Kn=Kn, dist_transform=lambda l: l ** 2)
        self.segMngr = PathSegmentManager(spline=self.splines)
        self.getRobotPosition = getRobotPositionLam
        self.setTargetHeading = setTargetHeadingLam
        self.currentSegment = self.splines[0]

    def update(self):
        robotPose: Point2D = self.getRobotPosition()
        self.segMngr.update(robotPoint=robotPose)
        self.gvf.setSpline(self.segMngr.getCurrentSegment())
        #self.gvf.setSpline(self.splines[0])
        self.currentSegment = self.gvf.spline
        self.setTargetHeading(self.gvf.getVectorFieldAtPoint(robotPose).normalizedGradient)

    def isFinished(self):
        return self.segMngr.completed()
